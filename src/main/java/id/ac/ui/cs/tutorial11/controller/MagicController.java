package id.ac.ui.cs.tutorial11.controller;

import id.ac.ui.cs.tutorial11.model.MagicModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import id.ac.ui.cs.tutorial11.service.MagicService;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/magic")
public class MagicController {

    @Autowired
    MagicService magicService;

    @GetMapping("/")
    public Flux<MagicModel> getAllMagic() {
        return magicService.findAll();
    }

    @PostMapping("/create/")
    public ResponseEntity createMagic(@RequestBody MagicModel magicModel) {
        magicService.addMagic(magicModel);
        return ResponseEntity.ok("Added new Magic");
    }

}   