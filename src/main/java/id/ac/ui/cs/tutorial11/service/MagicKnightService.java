package id.ac.ui.cs.tutorial11.service;

import id.ac.ui.cs.tutorial11.model.MagicKnightModel;
import reactor.core.publisher.Flux;

public interface MagicKnightService {
    Flux<MagicKnightModel> findAll();
    MagicKnightModel addKnight(MagicKnightModel knight);
}