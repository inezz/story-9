package id.ac.ui.cs.tutorial11.service;

import id.ac.ui.cs.tutorial11.model.MagicModel;
import reactor.core.publisher.Flux;

public interface MagicService {
    Flux<MagicModel> findAll();
    MagicModel addMagic(MagicModel magic);
}